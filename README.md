# Code Samples _(code-samples)_

> Code samples that I often show and discuss during my lecture sessions.

## License

This work is licensed under [Creative Commons Attribution-ShareAlike 4.0 International][1].

[1]: LICENSE